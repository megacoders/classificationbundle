<?php

namespace Megacoders\ClassificationBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\ClassificationBundle\Admin\CategoryAdmin as BaseAdmin;

class CategoryAdmin extends BaseAdmin
{

    /**
     * {@inheritdoc}
     */
    public function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper
            ->with('Options')
                ->add('eventDate', 'sonata_type_datetime_picker', [
                    'label' => 'admin.entities.category.event_date',
                    'required' => false,
                    'format' => 'yyyy-MM-dd HH:mm'
                ])
            ->end()
        ;
    }

}
