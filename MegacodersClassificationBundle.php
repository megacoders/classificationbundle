<?php
namespace Megacoders\ClassificationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;


class MegacodersClassificationBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataClassificationBundle';
    }
}