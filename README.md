- install *sonata-project/easy-extends-bundle* (if not yet)
- include in *AppKernel.php*
```
new Sonata\ClassificationBundle\SonataClassificationBundle(),
new Megacoders\ClassificationBundle\MegacodersClassificationBundle(),
```
- include in *config.yml*
```
sonata_classification:
    class:
        tag:          Megacoders\ClassificationBundle\Entity\Tag
        category:     Megacoders\ClassificationBundle\Entity\Category
        collection:   Megacoders\ClassificationBundle\Entity\Collection
        context:      Megacoders\ClassificationBundle\Entity\Context
```
```
doctrine:
    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        mappings:
            MegacodersClassificationBundle: ~            
```
- generate database structure
- fix *sonata-project/classification-bundle* from *sonata-patch.tar.gz* archive