<?php

namespace Megacoders\ClassificationBundle\Entity;

use Megacoders\ClassificationBundle\Model\Category;

abstract class BaseCategory extends Category
{
    public function disableChildrenLazyLoading()
    {
        if (is_object($this->children)) {
            $this->children->setInitialized(true);
        }
    }
}
