<?php

namespace Megacoders\ClassificationBundle\Entity\Translations;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\ClassificationBundle\Entity\Category;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="classification__category_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_classification__category_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class CategoryTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\ClassificationBundle\Entity\Category", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Category
     */
    protected $object;

}
